<?php

namespace App\modelos;

use Illuminate\Database\Eloquent\Model;

class lista_ua extends Model
{
    protected $table = "lista_ua";
    protected $primaryKey = "id_lista_ua";
    protected $fillable = ["lista_ua"];
    public $timestamps = false;
}
