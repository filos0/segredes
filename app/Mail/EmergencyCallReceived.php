<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmergencyCallReceived extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct($Data)
    {
        $this->Data = $Data;
    }

    public function build()
    {
        return $this->view('emergency_call')
            ->with("Data",$this->Data);
    }
}
