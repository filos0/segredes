<?php

namespace App\modelos;

use Illuminate\Database\Eloquent\Model;

class incidencias_modelo extends Model
{
    protected $table = "incidencia";
    protected $primaryKey = "id_incidencia";
    protected $fillable = ["descripcion","ataque","email_id","ip_ataque","fecha"];
    public $timestamps = false;
}
