@extends('layouts.master')

@section('content')
    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(
                    function (isConfirm) {
                        if (isConfirm) {
                            $('[name = correo]').val("").focus();
                        }
                    });
            </script>
        @endforeach

    @endif

    <div class="panel panel-flat">

        <div class="panel-heading">
            <span style="font-size: 32px" class="text-teal-800"><b>Estos son las amenzas que se han detectado <i
                            style="font-size: 28px;color: red" class="icon icon-fire"></i> </b></span><br>
            <span style="font-size: 28px"></span>
        </div>
        <div class="panel-body">
            <div class="col-md-12" style="margin-top:1%">


                @foreach($logs as $log)
                    <div class="col-md-6 ">
                        <div class="panel bg-danger-800">
                            <div class="panel-body">
                                <div class="text-center">
                                    <label style="font-size: 28px">
                                        <b>IP Atacante : {{$log->ip}}</b></label> <br>


                                </div>
                                <div class="text-left">
                                    <label style="font-size: 22px" ><b>User-Agent :</b> {{$log->user_agent}} </label><br>
                                    <label style="font-size: 22px" ><b>Método :</b> {{$log->metodo}} </label>
                                </div>
                            </div>
                            <div class="panel-footer bg-danger text-right">
                                <div class="">
                                    <b> Fecha: {{$log->fecha}} </b>
                                </div>
                            </div>
                        </div>

                    </div>
                @endforeach

            </div>
        </div>
    </div>

@endsection
