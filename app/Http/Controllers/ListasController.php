<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\modelos\contacto_modelo as Contactos;
use App\modelos\lista_ua;
use App\modelos\lista_ip;

class ListasController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index_ip(Request $request)
    {
        $lista = json_decode(lista_ip::all()->first()->ips);
        return view('lista_ip')->with("ips", $lista);
    }

    public function index_ua(Request $request)
    {
        $lista = json_decode(lista_ua::all()->first()->lista_ua);

        return view('lista_ua')->with("uas", $lista);
    }


}
