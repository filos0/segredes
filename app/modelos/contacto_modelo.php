<?php

namespace App\modelos;

use Illuminate\Database\Eloquent\Model;

class contacto_modelo extends Model
{
    protected $table = "email";
    protected $primaryKey = "id_email";
    protected $fillable = ["email","nombre"];
    public $timestamps = false;
}
