<?php

namespace App\Http\Middleware;

use App\Mail\EmergencyCallReceived;
use Illuminate\Support\Facades\Mail;
use App\modelos\lista_ip as lista_ip;
use App\modelos\lista_ua as lista_ua;
use Closure;
use App\modelos\logs_modelo;
use App\modelos\contacto_modelo as contactos;

class RestrictIpMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function enviar_email($ip, $ua, $metodo)
    {
        $data = [
            "ip_ataque" => $ip,
            "fecha" => date('Y-m-d h:i:s'),
            "descripcion" => "Un posible ataque fue detectado con el user-agent " . $ua . " dentro de las peticiones que se realizaron al sistema",
            "tipo_ataque" => "Ataque con software malicioso",

        ];
        $data = new EmergencyCallReceived($data);

        foreach (contactos::all() as $contacto) {
            Mail::to($contacto->email)->send($data);
        }

    }

    public function handle($request, Closure $next)
    {


        $this->buscar_ip($request->ip(), $request->userAgent(), $request->method());
        $this->valida_ua($request->userAgent(), $request->ip(), $request->method());
        $this->buscar_ua($request->userAgent(), $request->ip(), $request->method());


        return $next($request);
    }

    public function buscar_ip($ip, $ua, $metodo)
    {

        $ips_denny = json_decode(lista_ip::all()->first()->ips);
        if (in_array($ip, $ips_denny)) {
            $this->enviar_email($ip, $ua, $metodo);


            $registro = new logs_modelo();
            $registro->ip = $ip;
            $registro->user_agent = $ua;
            $registro->metodo = $metodo;
            $registro->fecha = date('Y-m-d h:m:i');
            $registro->save();
            header('HTTP/1.0 403 Forbidden');
            die();
        } else
            return false;
    }

    public function valida_ua($ua, $ip, $metodo)
    {

        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) $res = 'Opera';
        elseif (strpos($user_agent, 'Edge')) $res = 'Edge';
        elseif (strpos($user_agent, 'Chrome')) $res = 'Chrome';
        elseif (strpos($user_agent, 'Safari')) $res = 'Safari';
        elseif (strpos($user_agent, 'Firefox')) $res = 'Firefox';
        elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) $res = 'Internet Explorer';
        else $res = 'Other';


        if ($res == "Other") {

            $this->enviar_email($ip, $ua, $metodo);

            $registro = new logs_modelo();
            $registro->ip = $ip;
            $registro->user_agent = $ua;
            $registro->metodo = $metodo;
            $registro->fecha = date('Y-m-d h:m:i');
            $registro->save();

             header('HTTP/1.0 403 Forbidden');
            die();
        } else
            return false;
    }

    public function buscar_ua($ua, $ip, $metodo)
    {

        $ua_denny = json_decode(lista_ua::all()->first()->lista_ua);

        if (in_array($ua, $ua_denny)) {

            $this->enviar_email($ip, $ua, $metodo);
            $registro = new logs_modelo();
            $registro->ip = $ip;
            $registro->user_agent = $ua;
            $registro->metodo = $metodo;
            $registro->fecha = date('Y-m-d h:m:i');
            $registro->save();
            header('HTTP/1.0 403 Forbidden');
            die();
        } else
            return false;
    }
}
