<?php

namespace App\modelos;

use Illuminate\Database\Eloquent\Model;

class lista_ip extends Model
{
    protected $table = "lista_ip";
    protected $primaryKey = "id_lista_ip";
    protected $fillable = ["ips"];
    public $timestamps = false;
}
