@extends('layouts.master')

@section('content')
    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(
                    function (isConfirm) {
                        if (isConfirm) {
                            $('[name = correo]').val("").focus();
                        }
                    });
            </script>
        @endforeach

    @endif

    <div class="panel panel-flat">

        <div class="panel-heading">
            <span style="font-size: 32px" class="text-teal-800"><b>Lista de User-Agents  <i
                            class="icon icon-users4"></i> </b></span><br>
            <span style="font-size: 28px">Estas listas Nunca se podran conectar a nuestro sistema</span>
        </div>
        <div class="panel-body">

            @foreach($uas as $ua)
                <div class="col-md-4">
                    <div class="panel">
                        <div class="panel-body text-center">
                            <span style="font-size: 18px" class="text-bold">{{$ua}}</span><br>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection
