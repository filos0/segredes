<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aplicación de Redes</title>
    <link rel="shortcut icon" type='image/x-icon' href="{{asset('/favicon.ico') }}"/>


    <link href="{{asset('/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/colors.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/sweetalert2.css')}}" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="{{asset('/assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/bootstrap.min.js')}}"></script>


    <script type="text/javascript" src="{{asset('/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/sweetalert2.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/echart.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/pages/datatables_responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('/assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>


</head>
<body>
<div class="navbar navbar-default header-highlight">
    <div class="navbar-header">
        <a class="navbar-brand" href="#"></a>
        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class=""></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li>
                <a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li>

                <b><a id="hora" class="navbar-brand"></a></b>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-left">
            <li>

                <a class="navbar-brand" style="padding: 0px;">
                    <img style="  height: 100%;padding: 5px;width: auto;"
                         src="{{asset('')}}" alt="">
                </a>

            </li>
        </ul>
        <ul class="nav navbar-nav navbar-left">
            <li>
                <a class="navbar-brand" style="padding: 0px;">
                    <!--img style="  height: 100%;padding: 5px;width: auto;" src="{asset('/assets/img/cedula.jpg')}}" alt=""-->
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">

            <div class="sidebar-content">
                <div class="sidebar-user" style="cursor: pointer">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img style="width: 60px;height: 60px"
                                                                src="{{asset('/assets/img/upi.png')}}"
                                                                class="" alt=""></a>
                            <div class="media-body">
                                <div class="text-center">
                                <span class="media-heading">
     <br>
                                   <b>UPIICSA - IPN</b>

                                </span>
                                    <div class="text-size-mini text-muted ">
                                        APLICACIÓN DE REDES
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="navbar-header"></div>

                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">

                            <li>
                                <a href="{{ url('/') }}" class=" legitRipple">
                                    <i class="icon-home4"></i>
                                    <span>Inicio</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/contactos') }}" class=" legitRipple">
                                    <i class="icon-users"></i>
                                    <span>Lista Contactos</span>
                                </a>
                            </li>


                            <li><a href="{{url('/Lista/User-Agent')}}"><i class="icon icon-list3"></i>
                                    <span>Lista de User-Agents</span></a>
                            </li>

                            <li>
                                <a href="{{url('/Lista/IP')}}"><i class="icon icon-list2"></i>
                                    <span>Lista de IP</span>
                                </a>
                            </li>


                            <li>
                                <a href="{{url('/Logs')}}"><i class="icon icon-eye8"></i>
                                    <span>Logs</span>
                                </a>
                            </li>

                          <!--  <li>
                                <a href="{{url('/Reportes/Hoy')}}"><i class="icon icon-fire"></i>
                                    <span>Incidentes</span>
                                </a>
                            </li>
-->

                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->
            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                @yield('content')

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>

</html>
