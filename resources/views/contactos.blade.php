@extends('layouts.master')

@section('content')
    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(
                    function (isConfirm) {
                        if (isConfirm) {
                            $('[name = correo]').val("").focus();
                        }
                    });
            </script>
        @endforeach

    @endif

    <div class="panel panel-flat">

        <div class="panel-heading">
            <span style="font-size: 32px" class="text-teal-800"><b>Contactos <i class="icon icon-users4"></i> </b></span><br>
            <span style="font-size: 28px">Aquí podras ver y/o agregar contactos para recibir los detalles de los ataques</span>
        </div>
        <div class="panel-body">
            <div class="col-md-12" style="margin-top: 3%">
                <div class="col-md-4">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="text-center">
                                <label style="font-size: 24px"><b>Agregar nuevo contacto</b></label>
                            </div>
                            <br>
                            <form action="{{url('/agregar/contacto')}}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label style="font-size: 20px" class="text-bold">Correo </label>
                                    <input type="email" name="correo" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label style="font-size: 20px" class="text-bold">Nombre Completo </label>
                                    <input type="text" name="nombre" class="form-control " required>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-xlg bg-danger-800">AGREGAR</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="text-center">
                                <label style="font-size: 24px"><b>Lista de Contactos</b></label>
                            </div>
                            <br>
                            <div class="table-responsive">
                                <table class="table table-hover" style="font-size: 18px">
                                    <thead>
                                    <th class="text-bold text-center">CORREO</th>
                                    <th class="text-bold text-center">NOMBRE</th>
                                    </thead>
                                    <tbody>
                                    @foreach($contactos as $contacto)
                                    <tr>
                                        <td class="text-center">{{$contacto->email}}</td>
                                        <td class="text-center">{{$contacto->nombre}}</td>
                                    </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
