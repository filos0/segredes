<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Inicio');
});


Route::get('/Lista/IP', 'ListasController@index_ip');
Route::get('/Lista/User-Agent', 'ListasController@index_ua');
Route::get('/Logs', 'LogsController@index');

Route::get('/contactos', 'ContactosController@index');
Route::post('/agregar/contacto', 'ContactosController@agregar_contacto');
