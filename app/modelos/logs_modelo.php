<?php

namespace App\modelos;

use Illuminate\Database\Eloquent\Model;

class logs_modelo extends Model
{
    protected $table = "log";
    protected $primaryKey = "id_log";
    protected $fillable = ["ip","user_agent","metodo","fecha"];
    public $timestamps = false;
}
