@extends('layouts.master')

@section('content')
    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(
                    function (isConfirm) {
                        if (isConfirm) {
                            $('[name = placa]').val("").focus();
                        }
                    });
            </script>
        @endforeach

    @endif

    <div class="panel panel-flat">

        <div class="panel-body">

            <div class="row">

                <div class="col-md-4">
                    <img src="{{asset('/assets/img/robot.png')}}" class="img-responsive" alt="">
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="text-center" style="font-size: 32px;margin-top: 5%">
                            <span class="text-teal-800 text-bold ">Bienvenido al Sistema </span>
                        </div>
                        <div style="font-size: 26px;margin-top: 5%">

                            <span>Aquí podras controlar y monitorear los posibles ataques, así como configurar la aplicación</span>

                            <span class="text-bold ">Recuerda que ya estas protegido contra amenazas</span>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 8%">
                        <div class="text-center">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="panel bg-teal">
                                    <div class="panel-body">
                                        <div class="text-center">
                                            <label style="font-size: 28px" >
                                                <b>Número de Posibles Ataques</b></label> <br>
                                            <label style="font-size: 32px" >{{\App\modelos\logs_modelo::all()->count()}} </label>

                                        </div>
                                    </div>
                                    <div class="panel-footer bg-teal-800 text-right">
                                        <div class="">
                                            <b>  </b>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>

    </div>

@endsection
