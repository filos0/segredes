<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\modelos\contacto_modelo as Contactos;

class ContactosController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(Request $request)
    {

        return view('contactos')->with("contactos",Contactos::all());
    }
    public function agregar_contacto(Request $request){

        $contacto = new Contactos();
        $contacto->nombre = $request->nombre;
        $contacto->email = $request->correo;
        $contacto->save();
        return redirect()->back()->withErrors(array('success', 'Contacto Agregado Correctamente', ''));

    }
}
